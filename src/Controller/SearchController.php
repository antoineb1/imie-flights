<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Airport;
use App\Entity\Flight;

class SearchController extends Controller
{
    /**
     * @Route("/search", name="app_search")
     * @Template("Search/results.html.twig")
     */
    public function search(Request $request)
    {   
        $em = $this->getDoctrine()->getManager();

        // On recherche l'aéroport associée à la ville
        $airportFrom = $em
            ->getRepository(Airport::class)
            ->findOneBy([
                "city" => $request->query->get("airport-from"),
            ]);

        $airportTo = $em
            ->getRepository(Airport::class)
            ->findOneBy([
                "city" => $request->query->get("airport-to"),
            ]);

        // findBy = tableau d'objet
        $flights = $em 
                ->getRepository(Flight::class)
                ->findBy([
                    "airportFrom" => $airportFrom,
                    "airportTo" => $airportTo,
                ]);

        return [
            "flights" => $flights,
        ];
    }


}

