<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Flight;
use App\Entity\Passenger;
use App\Entity\Reservation;
use App\Form\ReservationType;

class BookingController extends Controller
{
    /**
     * @Route("/flight/{id}/booking", name="app_flight_booking")
     * @Template("Booking/booking.html.twig")
     * @ParamConverter("flight", class=Flight::class)
     */
    public function booking(Flight $flight, Request $request)
    {   
        $em = $this->getDoctrine()->getManager();
    
        $reservation = new Reservation();
        
        $passengers = [];
        for ($i=0; $i < $request->query->get("passengers"); $i++) { 
           $passenger = new Passenger();
           $passengers[] = $passenger;
        }

        $reservation->setPassengers($passengers);

        $form = $this->createForm(ReservationType::class, $reservation);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($reservation);
            $em->flush();

            exit;
        }

        return [
            "form" => $form->createView(),
            "flight" => $flight,
        ];  

    }



}

