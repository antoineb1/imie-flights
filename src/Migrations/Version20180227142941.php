<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180227142941 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fruit DROP FOREIGN KEY FK_A00BD29712469DE2');
        $this->addSql('ALTER TABLE purchase_fruit DROP FOREIGN KEY FK_74C051BDBAC115F0');
        $this->addSql('ALTER TABLE purchase_fruit DROP FOREIGN KEY FK_74C051BD558FBEB9');
        $this->addSql('CREATE TABLE airport (id INT AUTO_INCREMENT NOT NULL, city VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE flight (id INT AUTO_INCREMENT NOT NULL, airport_from_id INT DEFAULT NULL, airport_to_id INT DEFAULT NULL, number NUMERIC(10, 2) DEFAULT NULL, date_from DATETIME DEFAULT NULL, date_to DATETIME DEFAULT NULL, duration NUMERIC(10, 2) DEFAULT NULL, price NUMERIC(10, 2) DEFAULT NULL, INDEX IDX_C257E60E504E723 (airport_from_id), INDEX IDX_C257E60E47AA5690 (airport_to_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE page (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, text VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, udapted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE passenger (id INT AUTO_INCREMENT NOT NULL, reservation_id INT DEFAULT NULL, civility VARCHAR(255) DEFAULT NULL, birth_date DATETIME DEFAULT NULL, first_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, INDEX IDX_3BEFE8DDB83297E7 (reservation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservation (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, flight_id INT DEFAULT NULL, number NUMERIC(10, 2) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, date_from DATETIME DEFAULT NULL, date_to DATETIME DEFAULT NULL, created_at DATETIME DEFAULT NULL, INDEX IDX_42C84955A76ED395 (user_id), INDEX IDX_42C8495591F478C5 (flight_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT FK_C257E60E504E723 FOREIGN KEY (airport_from_id) REFERENCES airport (id)');
        $this->addSql('ALTER TABLE flight ADD CONSTRAINT FK_C257E60E47AA5690 FOREIGN KEY (airport_to_id) REFERENCES airport (id)');
        $this->addSql('ALTER TABLE passenger ADD CONSTRAINT FK_3BEFE8DDB83297E7 FOREIGN KEY (reservation_id) REFERENCES reservation (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C84955A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C8495591F478C5 FOREIGN KEY (flight_id) REFERENCES flight (id)');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE fruit');
        $this->addSql('DROP TABLE purchase');
        $this->addSql('DROP TABLE purchase_fruit');
        $this->addSql('ALTER TABLE user DROP grade');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE flight DROP FOREIGN KEY FK_C257E60E504E723');
        $this->addSql('ALTER TABLE flight DROP FOREIGN KEY FK_C257E60E47AA5690');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C8495591F478C5');
        $this->addSql('ALTER TABLE passenger DROP FOREIGN KEY FK_3BEFE8DDB83297E7');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fruit (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci, color VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, price NUMERIC(10, 2) DEFAULT NULL, created_at DATETIME NOT NULL, description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, INDEX IDX_A00BD29712469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchase (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL, price NUMERIC(10, 2) DEFAULT NULL, status TINYINT(1) NOT NULL, state TINYINT(1) NOT NULL, INDEX IDX_6117D13BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE purchase_fruit (id INT AUTO_INCREMENT NOT NULL, fruit_id INT DEFAULT NULL, purchase_id INT DEFAULT NULL, quantity INT NOT NULL, price NUMERIC(10, 2) NOT NULL, INDEX IDX_74C051BDBAC115F0 (fruit_id), INDEX IDX_74C051BD558FBEB9 (purchase_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fruit ADD CONSTRAINT FK_A00BD29712469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE purchase ADD CONSTRAINT FK_6117D13BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE purchase_fruit ADD CONSTRAINT FK_74C051BD558FBEB9 FOREIGN KEY (purchase_id) REFERENCES purchase (id)');
        $this->addSql('ALTER TABLE purchase_fruit ADD CONSTRAINT FK_74C051BDBAC115F0 FOREIGN KEY (fruit_id) REFERENCES fruit (id)');
        $this->addSql('DROP TABLE airport');
        $this->addSql('DROP TABLE flight');
        $this->addSql('DROP TABLE page');
        $this->addSql('DROP TABLE passenger');
        $this->addSql('DROP TABLE reservation');
        $this->addSql('ALTER TABLE user ADD grade VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
